\documentclass[english,11pt]{beamer}
\usepackage[T1]{fontenc}
%\usepackage[latin9]{inputenc}
\usepackage{natbib}
\usetheme{uzhneu-en}
\usepackage{float}
\usepackage{amsmath}
\usepackage{cancel}
\usepackage{amsthm}
\usepackage{enumerate}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{tikzit}
\input{sample.tikzstyles}

\usepackage{babel}


\begin{document}
%% Optional Argument in [Brackets]: Short Title for Footline
\title[Fracture load of all-ceramic systems: Bayesian synthesis of evidence ]{Fracture load of all-ceramic systems: Bayesian synthesis of evidence based on similar dental material studies}

\subtitle{STA390 Statistical Practice }

\vspace{3mm}
\author{Martina Scauda \\ \vspace{3mm} Supervisor PD Dr. Malgorzata Roos}


\date{23/08/2022}


\maketitle

% chapter division
%\part{Mamma}


\begin{frame}{Motivation}
	\begin{itemize}
		\Large
		\item 3R Initiative \\
		Reduce \\
		Reuse \\
		Recycle
		\vspace{0.5mm}
		
		\item Similar laboratory condition
		
		\vspace{0.5mm}
		
		\item Pooling investigation 
		
		\end{itemize}
\end{frame}

\begin{frame}{Data}
	\begin{itemize}
		\large
		\item Voss fracture load (Newton)
		\item Crown specimens (Triceram, Zirox, VITA VM9)
		\item Study 1: \cite{Stawarczyk_Jahn_Becker_Fischer_Hammerle_2008} \\
		Study 2: \cite{Stawarczyk_Ozcan_Roos_Trottmann_Sailer_Hammerle_2011}\\
		Study 5: \cite{Stawarczyk_Ozcan_Hallmann_Roos_Trottmann_Hammerle_2012}\\ 
		Study 11: \cite{StawarczykJahn_Becker_Hammerle_2008}
		\vspace{0.5mm}
		 
		\item Combine data from different studies
	\end{itemize}
\end{frame}

\begin{frame}{Fracture Load Measurament}
	
	\begin{minipage}{2in}
		\includegraphics[width=\linewidth]{figures/Fig1_VossShearTest}
		\captionof*{figure}{Voss Shear Test}
	\end{minipage}
	\hfill
	\begin{minipage}{2in}
	\includegraphics[width=\linewidth]{figures/VossTestDesign}
	\captionof*{figure}{Voss test design}
	\end{minipage}
	
	\vspace{0.5cm}
\end{frame}

\begin{frame}{Failure type}
	
	\begin{minipage}{2in}
		\includegraphics[width=\linewidth]{figures/Fig2_VossTotalFracture}
		\captionof*{figure}{Voss Total Fracture}
	\end{minipage}
	\hfill
	\begin{minipage}{2in}
		\includegraphics[width=\linewidth]{figures/Fig3_VossChipping}
		\captionof*{figure}{Voss Chipping}
	\end{minipage}
	
	\end{frame}

\begin{frame}{Violin Plots}
\begin{figure}
	\centering
	\includegraphics[width=0.6\linewidth]{figures/Violin1-1}
	\caption*{\footnotesize Violin plot of fracture load per veneering ceramic 1}
\end{figure}
\end{frame}


\begin{frame}{Original Pocock's conditions (\cite{Pocock_1976}) }
	\small
	\begin{enumerate}[i]
		\item \textit{The historical control group must have received a precisely defined standard treatment which must be the same as the treatment for the randomized controls.}
		\item \textit{The group must have been part of a recent clinical study which contained the same requirements for patient eligibility.}
		\item \textit{The methods of treatment evaluation must be the same.}
		\item \textit{The distributions of important patient characteristics in the group should be comparable with those in the new trial.}
		\item \textit{The previous study must have been performed in the same organization with largely the same clinical investigators}
		\item \textit{There must be no other indications leading one to expect differing results between the randomized and historical controls}
	\end{enumerate}
\end{frame}

\begin{frame}{Pocock's conditions for dental material studies}
		\small
		\begin{enumerate}[i]
			\item The materials and techniques used to produce veneering ceramic are standardized between different analyzed studies.  
			\item The historical veneering ceramics have been part of a dental material study with the same preparation technique for specimens.
			\item The methods of veneering ceramic quality evaluation must be the same. 
			\item The distributions of important specimen characteristics in the studies should be comparable. 
			\item The historical studies must have been performed in the same organization with largely the same clinical investigators.  
			\item There must be no other indications leading one to expect differing results between the future and historical studies. 
		\end{enumerate}
\end{frame}

\begin{frame}{Summary of external evidence}
	\begin{itemize}
		\Large
		\item Notation: $\theta_h, h = 1, \dots, H$, $\bar y_h$,  $\sigma_h = \hat{\sigma}_h$ 
		\item Irrelevance
		\item Exchangeable 
		\item Direct pooling
	\end{itemize}  
(\cite{Spiegelhalter_Abrams_Myles_2003})
	\end{frame}

\begin{frame}{Irrelevance}
	\small	
	Data: $y_{1h}, \dots, y_{n_hh} \sim N(\theta_h, \sigma^2_h) \quad \forall h=1, \dots, H$
	\vspace{1.5mm}
	
	Prior: $\theta_h \sim N(\mu, \tau^2) \quad \forall h=1, \dots, H$ 
	\vspace{1.5mm}
	
	Posterior distribution $\forall h =1, \dots, H$: 
	
	\begin{equation*}
	\theta_h | y_{1h}, \dots, y_{n_h h} \sim N\Bigg (\frac{\frac{n_h \bar y_h}{\sigma_h^2} + \frac{\mu}{\tau^2}}{\frac{n_h}{\sigma_h^2}+ \frac{1}{\tau^2}}, \Big( \frac{n_h}{\sigma_h^2}+\frac{1}{\tau^2}\Big)^{-1}\Bigg)
	\end{equation*}
	
	Posterior predictive distribution $\forall h =1, \dots, H$: 
	\begin{equation*}\label{eq:metirrpred}
	y_{(n_{h} + 1)h} |y_{1h}, \dots, y_{n_h h} \sim N\Bigg (\frac{\frac{n_h \bar y_h}{\sigma_h^2} + \frac{\mu}{\tau^2}}{\frac{n_h}{\sigma_h^2}+ \frac{1}{\tau^2}}, \Big( \frac{n_h}{\sigma_h^2}+\frac{1}{\tau^2}\Big)^{-1} + \sigma_h^2\Bigg)
	\end{equation*}
	\end{frame}
\begin{frame}{Irrelevance assumption}
		\ctikzfig{irrelevance}	
\end{frame}

\begin{frame}{Results}
	\begin{figure}
		\centering
		\includegraphics[width=1.1\linewidth]{figures/IrrelevancePlot}
	\end{figure}
	\end{frame}

\begin{frame}{Exchangeable}
	Bayesian normal-normal hierarchical model (NNHM).
	\vspace{1.5mm}
	
	Data: $y_{1h}, \dots, y_{n_hh} \sim N(\theta_h, \sigma^2_h) \quad \forall h=1, \dots, H$
	\vspace{1.5mm}
	
	Random effects $\theta_h \sim N(\mu, \tau^2) \quad \forall h =1, \dots, H.$
	\vspace{1.5mm}
	
	Prior: $\mu \sim N(\nu,\gamma^2), \quad \tau \sim \text{HN}(A)$
	\vspace{1.5mm}
	
	 Fit with \texttt{bayesmeta} (\cite{Rover_2020})
\end{frame}
\begin{frame}{Exchangeable assumption}
	\ctikzfig{exchangeable}	
\end{frame}

\begin{frame}{Results}
	\begin{figure}
		\centering
		\includegraphics[width=1.1\linewidth]{figures/ExchangeablePlot}
	\end{figure}
\end{frame}

\begin{frame}{Direct Pooling}
	\small
	Data: $y_{1h}, \dots, y_{n_hh} \sim N(\theta, \sigma^2_h) \quad \forall h=1, \dots, H$
	\vspace{1.5mm}
	
	Prior: $\theta \sim N(\nu, \gamma^2)$ 
	\vspace{1.5mm}
	
	Posterior distribution: 
	
	\begin{equation*}\label{eq:metpoolpost}
	\theta | \mathbf{y}_1, \dots, \mathbf{y}_{\tilde h} \sim N \Bigg (\frac{\sum_{h=1}^{\tilde h} \frac{n_h\bar y_h}{\sigma_h^2}+ \frac{\nu}{\gamma^2}}{\sum_{h=1}^{\tilde h} \frac{n_h}{\sigma_h^2}+ \frac{1}{\gamma^2}}, \Bigg (\sum_{h=1}^{\tilde h} \frac{n_h}{\sigma_h^2} + \frac{1}{\gamma^2} \Bigg)^{-1}\Bigg)
	\end{equation*}
	
	Posterior predictive distribution $\forall \, \tilde h \in \{1, \dots, H\}:$
	\begin{equation*}\label{eq:metpoolpred}
	y_{(n_{\tilde h}+1)\tilde h} \mid \mathbf{y}_1, \dots, \mathbf{y}_{\tilde h} \sim N\Bigg (\frac{\sum_{h=1}^{\tilde h} \frac{n_h\bar y_h}{\sigma_h^2}+ \frac{\nu}{\gamma^2}}{\sum_{h=1}^{\tilde h} \frac{n_h}{\sigma_h^2}+ \frac{1}{\gamma^2}}, \Bigg (\sum_{h=1}^{\tilde h} \frac{n_h}{\sigma_h^2} + \frac{1}{\gamma^2} \Bigg)^{-1} + \sigma^2_{\tilde h}\Bigg)
	\end{equation*}
\end{frame}

\begin{frame}{Direct pooling assumption}
	\ctikzfig{directpooling}	
\end{frame}
	
\begin{frame}{Results}
	\begin{figure}
		\centering
		\includegraphics[width=1.1\linewidth]{figures/DirectPoolingPlot}
	\end{figure}
\end{frame}

\begin{frame}{Posterior mean approaches}
	\begin{figure}
		\centering
		\includegraphics[width=1\linewidth]{figures/ComPLot1meanpre}
		\captionof*{figure}{\centering \tiny Comparison of different pooling approaches for fracture load per veneering ceramic 1}
	\end{figure}
\end{frame}

\begin{frame}{Posterior predictive approaches}
	\begin{figure}
		\centering
		\includegraphics[width=1\linewidth]{figures/ComPlot1predictive}
		\captionof*{figure}{\centering \tiny Comparison of different pooling approaches for fracture load per veneering ceramic 1}
	\end{figure}
\end{frame}



\begin{frame}{Discussion}

	\textbf{Limitations}
	
	\begin{itemize}
		\item Lack of sensitivity analysis
		\item Main focus on total fracture and assumption of normality 
		\item Use of estimates $\hat{\sigma}$ of the within-study standard deviations
	\end{itemize}

\textbf{Contribution}
\begin{itemize}
	\item Reduce the number of required observations 
	\item DP close to frequentist case
	\item Individual data not necessary
	\item Translation of Pocock's conditions
\end{itemize}
\end{frame}


\begin{frame}{Bibliography}
\tiny
\bibliographystyle{apalike}
\bibliography{reference}
	
\end{frame}

\begin{frame}{}
	\Huge \centering \usebeamercolor[fg]{title in head}\textbf{Thank you for \\your attention!}
	\end{frame}

\end{document}
