\documentclass[english,9pt]{beamer}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage[authoryear]{natbib}
\usetheme{uzhneu-en}
\setbeamertemplate{itemize item}[ball]
\usepackage{babel}
\usepackage{subcaption}
\usepackage{pgf}
\usepackage{hyperref}
\hypersetup{
	colorlinks=true,
	linkcolor=blue,
	filecolor=magenta,      
	urlcolor=cyan,
}
\makeatletter
\newcommand*{\rom}[1]{\expandafter\@slowromancap\romannumeral #1@}
\makeatother
\usepackage{tikz}
\usetikzlibrary{mindmap,trees, matrix}
\tikzset{every node/.append style={scale=1.5}}
\usepackage{amsmath, amssymb, bm}     % for nice mathematics
\usepackage{geometry}
\usepackage{changepage} % for adjustwidth in tabulars
\usepackage{multicol} %for two columns in references
\usepackage{xcolor, colortbl} %%%colorful text
\definecolor{maroon}{rgb}{128,0,0}

% \AtBeginSection[]{
%   \begin{frame}
%   \vfill
%   \centering
%   \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
%     \usebeamerfont{title}\secname\par%
%   \end{beamercolorbox}
%   \vfill
%   \end{frame}
% }
\usepackage{algorithm2e}
%\usepackage{beamerthemesplit}%for backup
\newcommand{\backupbegin}{
   \newcounter{finalframe}
   \setcounter{finalframe}{\value{framenumber}}
}
\newcommand{\backupend}{
   \setcounter{framenumber}{\value{finalframe}}
}


%\setbeamertemplate{page number in head/foot}[appendixframenumber]

\begin{document}

%% Optional Argument in [Brackets]: Short Title for Footline

\title[Biostatistics retreat, sona.hunanyan@uzh.ch, malgorzata.roos@uzh.ch]{Sensitivity diagnostics for Bayesian hierarchical models}

\subtitle{Biostatistics Mini Department Retreat

03.12.2021}

\author{Sona Hunanyan and Ma{\l}gorzata Roos}


\date{03.12.2021} %\today

\maketitle

% \begin{frame}[c]
% \titlepage
% \end{frame}


\begin{frame}{SEDA: Sensitivity diagnostics for Bayesian hierarchical models}
\begin{itemize}
  \item SNSF project (\#175933) 
  \vspace{3mm}
  \item Duration: 2018--2021
  \vspace{3mm}
 \item Team: \begin{itemize}
  \item PhD student: Sona Hunanyan
  \vspace{3mm}
  \item Post Doc: Manuela Ott
  \vspace{3mm}
  \item Assistant: Georgios Kazantzidis
  \vspace{3mm}
  \item Principal investigator: PD Dr. Ma{\l}gorzata Roos
 \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Project I}
\begin{columns}[t]
\begin{column}{0.6\textwidth}
% \vspace{-1cm}
\textcolor{uzh@blue}{Sensitivity and identification quantification by a relative latent model complexity perturbation in Bayesian meta-analysis}
\vspace{3mm}
% \begin{center}
\begin{itemize}
  % \vspace{3mm}
  \item Ma{\l}gorzata Roos, Sona Hunanyan, Haakon Bakka, H\aa vard Rue
  % \vspace{3mm}
  \item Biometrical Journal, 2021 (accepted)
  % \vspace{3mm}
  \item \url{https://doi.org/10.1002/bimj.202000193}
  % \vspace{3mm}
  \item Package: \texttt{si4bayesmeta} (\url{https://github.com/hunansona/si4bayesmeta})
\end{itemize}
% \end{center}
\end{column}
\begin{column}{0.49\textwidth}
\vspace{-1.5cm}
\begin{figure}
\includegraphics[scale = 0.3]{figures/paper1.png}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Project II}
\textcolor{uzh@blue}{Quantification of empirical determinacy: The impact of likelihood weighting on posterior location and spread in Bayesian meta-analysis estimated with JAGS and INLA}
\vspace{3mm}
\begin{itemize}
  % \vspace{3mm}
  \item Sona Hunanyan, H\aa vard Rue, Martyn Plummer, Ma{\l}gorzata Roos
  % \vspace{3mm}
  \item Bayesian Analysis (under review)
  % \vspace{3mm}
  \item \url{https://arxiv.org/abs/2109.11870}
  % \vspace{3mm}
  \item Package: \texttt{ed4bhm} (\url{https://github.com/hunansona/ed4bhm})
\end{itemize}
\vspace{-5mm}
\begin{figure}
\includegraphics[scale = 0.55]{figures/paper2.png}
\end{figure}
\end{frame}

\begin{frame}{Project III}
\begin{columns}[t]
\begin{column}{0.55\textwidth}
\textcolor{uzh@blue}{Empirical determinacy in Bayesian logistic regression in the presence of separation in the data}
\vspace{3mm}
\begin{itemize}
  % \vspace{3mm}
  \item Sona Hunanyan, Georgios Kazantzidis, H\aa vard Rue, Ma{\l}gorzata Roos
  % \vspace{3mm}
  \item In preparation for Biometrika
  % \vspace{3mm}
  \item Updated package: \texttt{ed4bhm} (\url{https://github.com/hunansona/ed4bhm})
\end{itemize}
\end{column}
\begin{column}{0.55\textwidth}
\vspace{-1.5cm}
\begin{figure}
\includegraphics[scale = 0.3]{figures/paper3.png}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Project IV}
\begin{columns}[t]
\begin{column}{0.55\textwidth}
\textcolor{uzh@blue}{Sensitivity-based identification of inaccurate heterogeneity priors in Bayesian meta-analysis}
\vspace{3mm}
\begin{itemize}
  % \vspace{3mm}
  \item Manuela Ott, Sona Hunanyan, Leonhard Held, Ma{\l}gorzata Roos
  % \vspace{3mm}
  \item In preparation for Statistics in Medicine
  % \vspace{3mm}
  \item Package: \texttt{pa4bayesmeta} (\url{https://r-forge.r-project.org/projects/pa4bayesmeta/})
\end{itemize}
\end{column}
\begin{column}{0.55\textwidth}
\vspace{-2mm}
\begin{figure}
\includegraphics[scale = 0.3]{figures/paper4.png}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Project V}
\textcolor{uzh@blue}{How vague is vague? How informative is informative? Reference analysis for Bayesian meta-analysis}
\vspace{3mm}
\begin{itemize}
  \item Manuela Ott, Martyn Plummer, Ma{\l}gorzata Roos
  \vspace{1mm}
  \item Statistics in Medicine, 2021, 40(20), 4505--4521
  \vspace{1mm}
  \item \url{https://doi.org/10.1002/sim.9076}
  \vspace{1mm}
  \item Package: \texttt{ra4bayesmeta} (\url{https://cran.r-project.org/package=ra4bayesmeta})
\end{itemize}
  \vspace{0.7cm}
ROeS 2021, Salzburg, oral presentation
\end{frame}
\end{document}

